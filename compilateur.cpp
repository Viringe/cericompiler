//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char current, AheadChar;				// Current car	

void ReadChar(void){		// Read character and skip spaces until 
				// non space character is read
	while(cin.get(current) && (current==' '||current=='\t'||current=='\n'))
	   	cin.get(current);
}

void ReadAheadChar(void){	//read ahead char	
	if(cin.peek() == '(')
	{
		return;
	}
	else if(cin.peek() == '>' or cin.peek() == '=')
	{
		ReadChar();
	}
	else if(cin.peek() ==' ' or cin.peek() =='\t'or cin.peek()=='\n')
	{
		ReadAheadChar();
		return;
	}
	//while(cin.peek(AheadChar) && (AheadChar==' ' or AheadChar=='\t'or AheadChar=='\n'));
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}

// ArithmeticExpression := Term {AdditiveOperator Term}
// Term := Digit | "(" ArithmeticExpression ")"
// AdditiveOperator := "+" | "-"
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"

	
void AdditiveOperator(void){
	if(current=='+'||current=='-')
		ReadChar();
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}

void EqualityOperator(void){
	if(current=='=' or current=='<' or current=='>')
		ReadChar();
	else
		Error("=,< ou > attendu");	   // equality operator expected
}

void ArithmetiqueOperator(void){
	if(current=='=' or current=='>')
		ReadChar();
	else
		Error("= ou > attendu car apres =,< on ne peut avoir que ceula");	   // equality operator expected
}
		
void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}

void ArithmeticExpression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		ArithmeticExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9')
			Digit();
		else
			Error("'(' ou chiffre attendu");
}

void ArithmeticExpression(void){
	char adop;
	char equa;
	char nextcar;
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
	
	}
	
	while(current=='=' or current=='<' or current=='>')
	{
		// '=' | '<>' | '<' | '<=' | '>=' | '>'  
		//equalité if a=b return true
		// siwtch case selon comparateur
		// jump a la fin
		equa=current;		// Save operator in local variable
		EqualityOperator();
		
		ReadAheadChar(); //read the seconcd carater LL(2) 
		if(current !='(')
		{
			ArithmetiqueOperator();
			
		}
		nextcar=current;
		//current=AheadChar;
		
		
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch (equa){
			case '=':
				cout << "\tcmpq	%rbx, %rax"<<endl;	// cheked if a=b
				cout << "\tje fin" << endl;
			break;
			case '<':
				if(nextcar == '>')
				{
					//<>
					cout << "\tcmpq	%rbx, %rax"<<endl;	// cheked if a<>b
					cout << "\tjne fin" << endl;
				}
				else if(nextcar == '=')
				{
					//<=
					cout << "\tcmpq	%rbx, %rax"<<endl;	// cheked if a<=b
					cout << "\tjb idd" << endl;
				}
				else
				{
					//below
					cout << "\tcmpq	%rbx, %rax"<<endl;	// cheked if a<b
					cout << "\tjb fin" << endl;
				}
				
			break;
			case '>':
				if(nextcar == '=')
				{
					//>=
					cout << "\tcmpq	%rbx, %rax"<<endl;	// cheked if a>=b
					cout << "\tjae fin" << endl;
				}
				else
				{
					//above
					cout << "\tcmpq	%rbx, %rax"<<endl;	// cheked if a>b
					cout << "\tja fin" << endl;
				}
			break;
		}
		
		
		
		
	}
	
		

}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	ArithmeticExpression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tpush %rax"<<endl;
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	cout << "fin:" << endl;
	cout << "\tpush %rax"<<endl;
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





